# UT Decision Maker

This is a repository for the UT Decision Maker project, a software which:
- analyses time evolution of those SALT registers which are crucial for the callibration process (to do)
- provides metrics quantifying the necessity of the next calibration (to do)
- predicts the timing of the next calibration (to do)
- ... 